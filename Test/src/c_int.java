/*
* Author: Wendell K Best
* Date: Feb 20, 2022
* c_int: C style representation of a C integer
*/

public class c_int{
	//C integers are 4 bytes
    private byte[] buf = new byte[4];

    //default constructor equals zero
    c_int(){
        for(int i = 0; i < 4; i++){
            buf[i]=0;
        }
    }

    // constructor creates four byte number
    c_int(int v){
        this.setValue(v);
    }
	//Getters and setters
    public int getSize(){return 4;} //the size of C int
    public int getValue(){
        int result = 0;
        result = (buf[0] << 24) + (buf[1] << 16) + (buf[2]<< 8) + buf[3];
        return result;
    } // the int value represented by buff
    public void setValue(byte[] b){
        for(int i = 0; i < 4; i++){
            buf[i] = b[i];
        }
    } //copy the value in b into buff
    public void setValue(int v){
        buf[0] = (byte)(v >> 24 & 0xFF);
        buf[1] = (byte)(v >> 16 & 0xFF);
        buf[2] = (byte)(v >> 8 & 0xFF);
        buf[3] = (byte)(v & 0xFF);
    } // set buf according to v
    public byte[] toByte(){return buf;} //return buff
}
