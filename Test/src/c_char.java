/*
* Author: Wendell K Best
* Date: 20 Feb 2022
* c_char : C style representation of a character
*/
public class c_char{
    private byte cChar; //c chars are one byte

    //c_char constructors
    c_char(){cChar = 0;}
    c_char(byte b){cChar = b;}
    c_char(char b){cChar = (byte) b;}
	//getters and setters
    public int getSize(){return 1;} //represents one byte
	//Java chars are 2 bytes
    public char getValue(){
        char temp = (char)(cChar & 0xFF);
        if(temp == 0)
            return ' ';
        else
            return temp;
    } //returns Java char
    public void setValue(byte b){cChar = b;}
    public void setValue(char c){cChar = (byte)c;}
    public byte toByte(){return cChar;}
}
