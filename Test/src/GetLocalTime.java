/*
* Author: Wendell K Best
* Date: Feb 20, 2022
* Test program
*/
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GetLocalTime {
     private c_int time;
     private c_char valid;
     private int bufsize = 104; //default CmdID and CmdLen

     //default constructor
     GetLocalTime(){
          time = new c_int();
          valid = new c_char();
     }
	//Constructor if you have the time stamp and valid char
     GetLocalTime(int number, char c){
          time = new c_int(number);
          valid = new c_char(c);
     }
	//Program to process Get Local Time command
     public int execute(String IP, int port) throws IOException, InterruptedException {
          //size of buffer
          int size = bufsize + time.getSize() + valid.getSize();
          //the buffer
          byte[] getloctime = new byte[size];
          //CmdID
          String getlocaltimeString = "GetLocalTime";
          //socket streams
          DataInputStream in=null;
          DataOutputStream out=null;

          //put CmdID in buffer
          for(int i = 0; i < getlocaltimeString.length();i++){
               getloctime[i] = (byte) getlocaltimeString.charAt(i);
          }
          //put buffer size in buffer
          getloctime[100] = (byte) (size >> 24);
          getloctime[101] = (byte) (size >> 16 & 0xFF);
          getloctime[102] = (byte) (size >> 8 & 0xFF);
          getloctime[103] = (byte) (size & 0xFF);

          //create socket and bind streams
          System.out.println("Creating socket for Get Local Time");
          try {
               Socket kkSocket = new Socket(IP, port);
               out = new DataOutputStream(kkSocket.getOutputStream());
               in = new DataInputStream(kkSocket.getInputStream());
          }catch (UnknownHostException e) {
               e.printStackTrace();
          } catch (IOException e) {
               e.printStackTrace();
          }

          //send the buffer to give size
          System.out.println("Sending package size");
          out.write(getloctime, 0, getloctime.length);
          out.flush();
          //send the bufferto request local time
          Thread.sleep(1000);
          System.out.println("Sending package");
          out.write(getloctime, 0, getloctime.length);
          out.flush();
          //receive the buffer
          in.readFully(getloctime);

          //see if correct buffer came back
          String GetLocalTime = new String(getloctime);
          //System.out.println("Received from server "+ GetLocalTime);
          if(GetLocalTime.contains(getlocaltimeString)){
               int times = (getloctime[bufsize] << 24) + (getloctime[bufsize + 1] << 16) + (getloctime[bufsize+2] <<8) + getloctime[bufsize+3];
               this.time.setValue(times);
               this.valid.setValue(getloctime[size-1]);
          }
          //if valid bit is false, failed to get local time
          if(valid.getValue() == 'f'){
               return 1;
          }
          return 0;
     }
	//Converts the C style time stamp to Java style and formats it
     String getLocalTime(){
          Date server = new Date(time.getValue() * 1000);
          SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
          String time = sdf.format(server);
          return time;
     }
	//getter and setters
     char getValid(){return valid.getValue();}
     void setValid(char c){valid.setValue(c);}
}

