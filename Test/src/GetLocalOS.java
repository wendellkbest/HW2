/*Author: Wendell K Best
* Date: Feb 20, 2022
* Get Local OS
*/

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import static java.sql.Types.NULL;

public class GetLocalOS {
    private ArrayList<c_char> OS;
    private c_char valid;
    private int bufsize = 104; //default CmdID and CmdLen
    private int OSsize = 16;

	//Default constructor	
    GetLocalOS(){
        OS =  new ArrayList<>();
        valid = new c_char();
    }

	//Constructor if you have the data
    GetLocalOS(byte[] newOS, char v){
        for(int i = 0; i < newOS.length; i++){
            OS.add(new c_char(newOS[i]));
        }
        valid = new c_char(v);
    }
	
	//Processes Get Local OS command
    public int execute(String IP, int port) throws IOException, InterruptedException {
	//initial buffer size
        int size = bufsize + OSsize + valid.getSize();
	//Buffer
        byte[] getos = new byte[size];
	//String for adding to package
        String getosString = "GetLocalOS";
	
	//Put the command ID in
        for(int i = 0; i < getosString.length(); i++){
            getos[i] = (byte) getosString.charAt(i);
        }
	//Put the size of the package in
        getos[100] = (byte) (size >> 24 & 0xFF);
        getos[101] = (byte) (size >> 16 & 0xFF);
        getos[102] = (byte) (size >> 8 & 0xFF);
        getos[103] = (byte) (size & 0xFF);
	
	//Create stream variables
        DataInputStream in=null;
        DataOutputStream out=null;
	
	//Connect socket to server
        try {
            Socket kkSocket = new Socket(IP, port);
            out = new DataOutputStream(kkSocket.getOutputStream());
            in = new DataInputStream(kkSocket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
	//Send command to deliver package size
        out.write(getos, 0, getos.length);
        out.flush();
	//Pause in case of delays
        Thread.sleep(1000);
	//Send package
        out.write(getos, 0, getos.length);
        out.flush();
	//Get Server response
        in.readFully(getos);
	
	//Add the server OS name character by character
        String GetOs = new String(getos);
        if(GetOs.contains(getosString)){
            for(int i = bufsize, j = 0; j < 16; i++, j++){
                OS.add(new c_char(getos[i]));
            }
        }
	//set the valid bit and check value
        valid.setValue(getos[bufsize + OS.size()]);
        if(valid.getValue() == 'f'){
            return 1;
        }
        return 0;
    }
	//Returns the OS name as a String, trims extra zeros
    public String getOS(){
        char[] arr = new char[16];
        char temp;
        for(int i = 0; i < 16; i++){
            arr[i] = OS.get(i).getValue();
        }
        return new String(arr).trim();
    }
	//Getter and setter for valid bit
    public char getValid(){
        return valid.getValue();
    }
    public void setValid(char c){valid.setValue(c);}
}
