/*
* Author: Wendell K Best
* Date: Feb 20, 2022
* Test program
*/
import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException, InterruptedException {
        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        System.out.println("Server IP " + hostName + " Port " + portNumber);
        //get local time from server
        System.out.println("Starting Get Local Time test");
        GetLocalTime test1 = new GetLocalTime();
        test1.setValid('f');
        if(test1.execute(hostName, portNumber)==1){
            System.out.println("Unable to get local time");
        }
        else{
            System.out.println("Time is " + test1.getLocalTime() + " Valid is " + test1.getValid());
        }

        System.out.println("Starting Get Local OS test");
        //get local OS from server
        GetLocalOS test2 = new GetLocalOS();
        test2.setValid('f');
        if(test2.execute(hostName, portNumber)==1){
            System.out.println("Unable to get Operating System Name");
        }
        else{
            System.out.println("Operating system is " + test2.getOS() + " Valid is " + test2.getValid());
        }
        System.out.println("All tests complete");
    }
}
