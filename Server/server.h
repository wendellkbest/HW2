//
// Created by wendellbest on 2/20/22.
//

#ifndef TEST_SERVER_H
#define TEST_SERVER_H

typedef  struct{
    int time;
    char valid;
}GET_LOCALTIME;

typedef struct{
    char OS[16];
    char valid;
}GET_LOCAL_OS;

void *CmdProcessor(void *socket);
void GetLocalTime(GET_LOCALTIME *ds);
void GetLocalOS(GET_LOCAL_OS *ds);

#endif //TEST_SERVER_H
