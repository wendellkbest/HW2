//
// Created by wendellbest on 2/20/22.
//
#include "server.h"
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/utsname.h>
#include <sys/socket.h>
#include <stdlib.h>
#define DEFAULT_BUFFER 130

//Function for processing commands from client
void *CmdProcessor(void *socket){
    int cSocket = *((int*)socket);
    int size = 0;
    char buffer[DEFAULT_BUFFER];
    char *GetLocalOSS = "GetLocalOS";
    char *GetLocalTimeS = "GetLocalTime";
	
    printf("Processing client request %d\n", cSocket);
    //Receive the size of the package
    if(recv(cSocket, buffer, DEFAULT_BUFFER, 0) == -1){
        perror("Receive size failed with error: ");
    }
    else{
        printf("Received size message from client:\n");
        /*for(int i = 0; i < DEFAULT_BUFFER; i++){
            printf("%c", buffer[i]);
        }
        printf("\n");*/
    }
    //determine size of package
    size = (buffer[100] << 24) + (buffer[101] << 16) + (buffer[102] << 8) + buffer[103];
    printf("Package size is %d\n",size);
    //receive the package
    if(recv(cSocket, buffer, size, 0) == -1){
        perror("Received message failed with error: ");
    }
    else{
        printf("Received package from client:\n");
        /*for(int i = 0; i < size; i++){
            printf("%c", buffer[i]);
        }
        printf("\n");*/
    };
	//Determine what command was sent
    if(strcmp(GetLocalTimeS, buffer)==0){
        printf("Processing command Get Local Time\n");
		//Create pointer to GET_LOCALTIME struct ds
        GET_LOCALTIME *ds;
		//Allocate memory
        ds = malloc(sizeof(GET_LOCALTIME));
		//Get the local time
        GetLocalTime(ds);
		//Put the local time integer in the array if true
        if(ds->valid == 't'){
            buffer[104] = 0xFF & (ds->time >> 24);
            buffer[105] = 0xFF & (ds->time >> 16);
            buffer[106] = 0xFF & (ds->time >> 8);
            buffer[107] = 0XFF & ds->time;
            buffer[size-1] = 't';
        }
		//if false
        else{
            buffer[size-1] = 'f';
        }
		//send the package
        if(send(cSocket, buffer, size, 0) == -1){
            perror("Send failed with error: ");
        }
        else{
            printf("Sent message Local Time\n");
            /*for(int i = 0; i < size; i++){
                printf("%c", buffer[i]);
            }
            printf("\n");*/
        }
	free(ds);
    }
	//Determine what command was sent
    if(strcmp(GetLocalOSS,buffer) == 0){
        printf("Processing command Get Local OS\n");
		//Create pointer to GET_LOCAL_OS struct ds
        GET_LOCAL_OS *ds;
		//Allocate memory
        ds = malloc(sizeof(GET_LOCAL_OS));
		//Get local OS
        GetLocalOS(ds);
		//If true, add OS to package and set true
        if(ds->valid == 't'){
            for(int i = 0, j =104; i < 16; i++, j++){
                buffer[j] = ds->OS[i];
            }
            buffer[size-1] = ds->valid;
        }
		//set false otherwise
        else{
            buffer[size-1] = 'f';
        }
		//send the package
        if(send(cSocket, buffer, size, 0) == -1){
            perror("Send failed with error: ");
        }
        else{
            printf("Sent message Local OS\n");
            /*for(int i = 0; i < size; i++){
                printf("%c", buffer[i]);
            }
            printf("\n");*/
        }
	free(ds);
    }
    printf("Finished processing request\n");
}

//Function to get the local time
void GetLocalTime(GET_LOCALTIME *ds){
    ds->time = time(0);
    if(ds->time > -1){
        ds->valid = 't';
        //printf("Sending local time of %d\n", ds->time);
    }
    else
        ds->valid = 'f';
}
//function to get the OS
void GetLocalOS(GET_LOCAL_OS *ds){
    struct utsname getos;
    if(uname(&getos) == 0){
        ds->valid = 't';
        strcpy(ds->OS, getos.sysname);
    }
    else
        ds->valid = 'f';
}
