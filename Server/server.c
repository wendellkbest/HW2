//
// Created by wendellbest on 2/20/22.
//
#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include "server.h"

int main(int args, char *argv[]) {
    int serverSocket, connsocket,  port;
    struct sockaddr_in servaddr;
	
	//Get port from command line
    port = atoi(argv[1]);
	
	//Create socket
    serverSocket = socket(AF_INET, SOCK_STREAM,0);
    if(serverSocket == -1){
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("socket successfully created..\n");
	
	//Create memory location for server
    memset(&servaddr, 0, sizeof(servaddr));
	//Add the server variable definitions
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);
	//Bind the sockaddr to the socket
    if((bind(serverSocket, (struct sockaddr*)&servaddr, sizeof(servaddr))) != 0){
        printf("Socket bind failed..\n");
        close(serverSocket);
        exit(0);
    }
    else
        printf("Socket successfully binded..\n");
	//Set socket to listen for requests
    if((listen(serverSocket, 5)) != 0){
        printf("Listen failed..\n");
        close(serverSocket);
        exit(0);
    }
    else
        printf("Server listening on port: %d\n",port);
    int counter = 0;
	//Loop to accept requests and create thread to process them
    while(1) {
        struct sockaddr client;
        unsigned int addrlen = sizeof(client);
        connsocket = accept(serverSocket, &client, &addrlen);
        if (connsocket < 0) {
            perror("Server accept failed: ");
        } else{
            printf("server accept the client..\n");
        printf("Connsocket is %d\n", connsocket);
        pthread_t thread;
        pthread_create(&thread,0, CmdProcessor, (void*)&connsocket);
        }
    }
    close(serverSocket);
}
