Author: Wendell K Best
Com S 487 Homework 2
Started: Feb 20, 2022
Finished: Feb 26, 2022

This program has two sets of code for the Server and Test client. Server is written in C and the Test client is in Java. A "Makefile" is included in the Server folder to compile it. The Java Test client can be done with, "javac Test.java".

The server needs to be started first and you must include the port it will be listening on when executing the program (i.e. ./server <port number>). The Test program requires the Server address and port as command line arguments.

SERVER:
The server program main code is in the server.c file and is what initially sets up the socket it listens on then awaits for connections. Any connections are accepted and given a new socket descripter and a processing thread is started. The processing thread receives the size of the package from the client then the package and then determines the command sent by the client. It then uses the required helper function get the operating system name or server local time and creates a package and sends it to the client then exits. The server program has two structs for GET_LOCAL_OS and GET_LOCALTIME which each have a char array to hold the data and the valid char.
The server can handle requests from different clients and each client can send a repeated request multiple times to the server.

ISSUES:
I could not use dynamic arrays to receive data from client, seems to be a thing for C programming and memory assignment that the function recv() can not use. I chose to use a sufficiently large array and then just made sure to only update the parts of the array according to the size of the package sent from the client. 

WHY USE COMMAND LINE INPUT: 
I wanted to be able to try the programs out between different computers and this seemed an easier way to do it with out changing the code itself.

TEST:
Test.java - Just gets the command line arguments and then creates two objects for "GetLocalTime" and "GetLocalOS", changes the valid to 'f', calls the execute method for each object, then prints out the results to the screen and exits.

GetLocalTime.java - Has the variables for a C style int and char. The class also has constructors, setters, and getters. It uses the methods from the c_int and c_char classes to perform additions, changes, and return values.

GetLocalOS.java - Has the variables for an array list of C style chars and char valid. The class also has constructors, setters, and getters. It uses the methods from the c_char class to perform additions, changes, and return values.

c_char.java - Class for respresenting a C style char. It has a byte representation of the character, constructors, getters and setters.

c_int.java - Class for respresenting a C style int. It has a byte array representation of an integer, constructors, getters and setters.

WHY USE AN ARRAY LIST IN GetLocalOS - Since the project required a "c_char" class, and ArrayList had convienient methods for managing a List of objects it seemed a good choice for a list of "c_char" objects. I wasn't able to create a "c_char" array as well, though it maybe because I need to create new c-char objects for each element in the array.